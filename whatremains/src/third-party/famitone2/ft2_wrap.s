.segment "CODE"

;$8000
jmp _ft2_initialize
;$8003
jmp _ft2_play_sfx
;$8006
jmp _ft2_flush
;$8009
_ft2_update:
  lda NSFDRIVER_PLAYER_FLAGS
  bne :+
  ; If nsfdriver is not playing anything, set all buffer volumes to 0.
  lda FT_OUT_BUF+0
  and #$f0
  sta FT_OUT_BUF+0
  lda FT_OUT_BUF+3
  and #$f0
  sta FT_OUT_BUF+3
  lda FT_OUT_BUF+6
  and #$f0
  sta FT_OUT_BUF+6
  lda FT_OUT_BUF+9
  and #$f0
  sta FT_OUT_BUF+9
:
  jmp FamiToneUpdate


_ft2_initialize:
  lda $0a ; tv-system
  eor #1
  ldx #$00
  ldy #$f0
  jsr FamiToneInit
  ldx #<sfx_data
  ldy #>sfx_data
  jmp FamiToneSfxInit

_ft2_play_sfx:
  ldx #0
  jmp FamiToneSfxPlay

_ft2_flush:
  jmp FamiToneFlush


FT_BASE_ADR		= $0100	;page in the RAM used for FT2 variables, should be $xx00
FT_TEMP			= $0d	;3 bytes in zeropage used by the library as a scratchpad
FT_DPCM_OFF		= $c000	;$c000..$ffc0, 64-byte steps
FT_SFX_STREAMS	= 1		;number of sound effects played at once, 1..4

FT_DPCM_ENABLE = 0			;undefine to exclude all DMC code
FT_SFX_ENABLE = 1			;undefine to exclude all sound effects code
FT_THREAD = 1				;undefine if you are calling sound effects from the same thread as the sound update call

FT_PAL_SUPPORT = 1			;undefine to exclude PAL support
FT_NTSC_SUPPORT = 1			;undefine to exclude NTSC support

.include "famitone2.s"

sfx_data:
.include "assets/music/sfx.s"


NSFDRIVER_PLAYER_FLAGS = $411
