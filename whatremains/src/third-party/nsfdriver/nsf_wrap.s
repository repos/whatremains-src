; $8000
jmp _internal_init_music
; $8003
jmp _internal_music_play
; $8006
jmp _internal_music_stop
; $8009
jmp _internal_music_update
; $800c
jmp _internal_music_sfx_play

_internal_init_music:
  lda #0
  sta var_PlayerFlags
  ldx TV_SYSTEM
  jmp ft_music_init

_internal_music_play:
  ldx TV_SYSTEM
  jmp ft_music_init

_internal_music_stop:
  lda #0
  sta var_PlayerFlags
  rts

_internal_music_update = ft_music_play

_internal_music_sfx_play:
  rts

.include "driver.s"

.export ft_music_addr
.export var_ch_DPCM_Offset
.export ft_read_pattern

DPCM_ADDR = $c000
DPCM_CHANNEL_OFFSET = ((DPCM_ADDR - $c000) >> 6)


MIXING_BUFFER = $170
TV_SYSTEM = $0a
