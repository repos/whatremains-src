;; -*- mode: lisp; -*-


(defsub (create-entity data-id)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-entity-bank)
    (set! ret (create-entity-wrapped data-id))
    (mmc1-prg-bank preserve-bank)
    ret))


(defsub (wait-entity data-id)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-entity-bank)
    (set! ret (wait-entity-wrapped data-id))
    (mmc1-prg-bank preserve-bank)
    (when (not ret)
      (old-yield))))


(defsub (hide-entity data-id)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-entity-bank)
    (set! ret (hide-entity-wrapped data-id))
    (mmc1-prg-bank preserve-bank)
    ret))


(defsub (modify-entity data-id)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-entity-bank)
    (set! ret (modify-entity-wrapped data-id))
    (mmc1-prg-bank preserve-bank)
    ret))


(defsub (text-box-update)
  (when (or (eq? tb-text-mode 0) (eq? tb-text-mode #xff))
    (return #f))
  (farcall text-box-update--inner))


(defsub (text-box-is-active)
  ; HACK
  (and tb-text-mode (< tb-text-mode #xff)))


(defsub (palette-assign id)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-extended)
    (set! ret (palette-assign--entry id))
    (mmc1-prg-bank preserve-bank)
    ret))


(defsub (palette-load id)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-extended)
    (set! ret (palette-load--entry id))
    (mmc1-prg-bank preserve-bank)
    ret))


(defsub (clear-crawl-text portion)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-nt-change-bank)
    ; TODO: Owned by crawl-text instead, dispatch based upon engine-mode.
    (cond
     ; Cinematic
     [(eq? engine-mode 0)
        (erase-cinematic-text--entry portion)]
     ; Gameplay
     [(eq? engine-mode 1)
        (text-box-erase--entry portion)]
     ; NES
     [(eq? engine-mode 3)
        (erase-cinematic-text--entry portion)])
    (set! text-cursor-v text-origin-v)
    (set! text-cursor-h text-origin-h)
    (set! text-cursor-nt text-origin-nt)
    (mmc1-prg-bank preserve-bank)))


(defsub (crawl-text-update)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-extended)
    (set! ret (crawl-text-update--entry))
    (mmc1-prg-bank preserve-bank)
    ret))


(defsub (render-object-fill-chr-ram-only id)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-renderable)
    (render-object-fill-chr-ram-only--entry id)
    (mmc1-prg-bank preserve-bank)))


(defsub (render-object id tile-offset)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-renderable)
    (render-object--entry id tile-offset)
    (mmc1-prg-bank preserve-bank)))


(defsub (render-object-nametable-update id tile-offset)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-renderable)
    (render-object-nametable-update--entry id tile-offset)
    (mmc1-prg-bank preserve-bank)))


(defsub (render-object-fill-chr-ram-buffered id)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-renderable)
    (render-object-fill-chr-ram-buffered--entry id)
    (mmc1-prg-bank preserve-bank)))


(defsub (choose-tune-for-scene)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-tune-mapping)
    (choose-tune-for-scene--entry)
    (mmc1-prg-bank preserve-bank)))


(defsub (add-entity-npc--wrapped npc-type v h)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-entity-bank)
    (set! ret (add-entity-npc npc-type v h))
    (mmc1-prg-bank preserve-bank)
    ret))


(defsub (change-to-scene id)
  (farcall load-scene-and-initialize id))


(defsub (run-cinematic-engine--wrapped)
  (let ((ret))
    (set! ret #f)
    (bank-switch-extended #t)
    (run-cinematic-engine)
    (block Cond
      (bcc #:break)
      (set! ret #t))
    (bank-switch-extended #f)
    ret))


(defsub (player-collide-with-entity--wrapped i)
  (let ((preserve-bank) (answer) (r0) (r1))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-entity-bank)
    (set-multiple! answer r0 r1 (player-collide-with-entity i))
    (mmc1-prg-bank preserve-bank)
    (return answer r0 r1)))


(defsub (update-entities--wrapped)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-entity-bank)
    (update-entities)
    (mmc1-prg-bank preserve-bank)))


(defsub (remove-entities-of-type--wrapped type)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-entity-bank)
    (remove-entities-of-type type)
    (mmc1-prg-bank preserve-bank)))


(defsub (draw-picture--wrapped v h pic attr)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-draw-pic-bank)
    (draw-picture v h pic attr)
    (mmc1-prg-bank preserve-bank)))


(defsub (draw-picture-jenny--wrapped v h pic attr b-v b-h b-m)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-draw-pic-bank)
    (draw-picture-jenny v h pic attr b-v b-h b-m)
    (mmc1-prg-bank preserve-bank)))


(defsub (draw-player--wrapped)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-code)
    (draw-player)
    (mmc1-prg-bank preserve-bank)))


(defsub (camera-scroll--wrapped)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-extended)
    (camera-scroll)
    (mmc1-prg-bank preserve-bank)))


(defsub (fill-attr-buffer--wrapped v nt page-num)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-code)
    (fill-attr-buffer v nt page-num)
    (mmc1-prg-bank preserve-bank)))


(defsub (fill-nt-buffer--wrapped v nt page-num)
  (let ((preserve-bank) (ret))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-code)
    (fill-nt-buffer v nt page-num)
    (mmc1-prg-bank preserve-bank)))


(defsub (travel-by-teleportation--wrapped to kind)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-code)
    (travel-by-teleportation to kind)
    (mmc1-prg-bank preserve-bank)))


(defsub (disable-exit-set-distance--wrapped i dist)
  (let ((preserve-bank))
    (set! preserve-bank curr-prg-bank)
    (mmc1-prg-bank prg-code)
    (disable-exit-set-distance i dist)
    (mmc1-prg-bank preserve-bank)))
