;; -*- mode: lisp; -*-

(defsub (nes-mini-game-scene)
  (let ((orig-ppu) (orig-v) (orig-h))
    (set! orig-ppu ppu-ctrl)
    (set! orig-v player-v)
    (set! orig-h player-h)
    (set! nes-player-jump 0)
    (catch (e)
      ; Body
      (do (nes-mini-game-run-it)
          (set! player-v orig-v)
          (set! player-h orig-h))
      ; Handler
      #f)
    (lda orig-ppu)
    (sta ppu-ctrl)
    (sta REG-PPU-CTRL)))


(defvarmem nes-mini-game-result #x108)
(defvarmem nes-mini-game-easier #x109)
(defvarmem nes-mini-game-ball-speed #x10a)
(defvarmem nes-ball-dir             #x10b)

(defsub (nes-mini-game-run-it)
  (music-stop)
  (set! nes-mini-game-result 0)
  (when debug-start-mini-game
    ; Magic number for NES cart graphics.
    (show-setup #x42 #f)
    (clear-nametables)
    (asm "jmp StartPlaying"))
  (set! text-blank 0)
  (set! font-bg 0)
  (set! font-invert #f)
  (text-origin 3)
  (clear-nametables)
  (assign-scroll)
  ; Opening
  (set! engine-cinema 2)
  (farcall init-cinematic-engine)
  (block LogoLoop
    (wait-frame)
    (read-joypad)
    (when (not (run-cinematic-engine--wrapped))
      (jmp LogoLoop)))
  (disable-screen)
  (asm "StartPlaying:")
  (when (>= progress-in-game progress-stage-6)
    (final-mission)
    (return))
  (setup-the-game)
  ; Start mini-game music
  (music-play-song audio-tune-mini-game)
  ; Main gameplay loop
  (play-the-game)
  (disable-screen)
  (when (eq? nes-mini-game-result 1)
    (set! engine-mode 1)
    (asm "jmp ExitNesScene"))
  ; Closing cart cinematic
  (let ((stage))
    (set! stage (current-stage-number))
    (cond
     [(eq? stage 1) (set! engine-cinema  3)]
     [(eq? stage 2) (set! engine-cinema  9)]
     [(eq? stage 3) (set! engine-cinema 14)]
     [(eq? stage 4) (set! engine-cinema 17)]
     [(eq? stage 5) (set! engine-cinema 19)]))
  (farcall init-cinematic-engine)
  (block ClosingLoop
    (wait-frame)
    (read-joypad)
    (when (not (run-cinematic-engine--wrapped))
      (jmp ClosingLoop)))
  (disable-screen)
  ; Return to gameplay scene.
  (set! generic-event 0)
  (fset flag-has-b-button)
  (set! engine-mode 1)
  (let ((stage))
    (set! stage (current-stage-number))
    (if (eq? stage 1)
        (set! generic-event 1)
        (set! generic-event 31))
    (cond
     [(eq? stage 1) (set! progress-in-game progress-played-nes-game)]
     [(eq? stage 2) (set! progress-in-game progress-played-nes-2)]
     [(eq? stage 3) (set! progress-in-game progress-played-nes-3)]
     [(eq? stage 4) (set! progress-in-game progress-played-nes-4)]
     [(eq? stage 5) (set! progress-in-game progress-played-nes-5)]))
  ; Exit NES scene.
  (asm "ExitNesScene:")
  ; Glitchy noisy exit sequence
  (let ((bank))
    (disable-screen)
    (clear-oam)
    (wait-frame)
    ; Load CHR
    (set! bank (resource-access exit-sequence-chr))
    (stx "my_pointer+0")
    (sty "my_pointer+1")
    (set! REG-PPU-ADDR (set! REG-PPU-ADDR #x00))
    (unrle-safe bank)
    ; Load nametable and attributes
    (set! bank (resource-access exit-sequence-gfx))
    (stx "my_pointer+0")
    (sty "my_pointer+1")
    (set! REG-PPU-ADDR #x20)
    (set! REG-PPU-ADDR #x00)
    (unrle-safe bank)
    ; Palette to black.
    (poke! palette-buffer 0 #x0f)
    (poke! palette-buffer 1 #x0f)
    (poke! palette-buffer 2 #x0f)
    (farcall palette-to-render-buffer-full)
    ; Sound effect
    (audio-play-sfx audio-sfx-exit-cart)
    ; Turn on screen.
    (wait-frame)
    (enable-screen)
    (loop n 0 7
          (exit-sequence-frame-0)
          (exit-sequence-frame-1)
          (exit-sequence-frame-2)))
  ; Shut it down.
  (music-stop)
  (clear-oam)
  (disable-screen))


(defsub (exit-sequence-frame-0)
  (poke! palette-buffer 0 #x19)
  (poke! palette-buffer 1 #x0f)
  (poke! palette-buffer 2 #x19)
  (farcall palette-to-render-buffer-full)
  (set! ppu-ctrl (or (and ppu-ctrl #xfc) #x00))
  (wait-frame)
  (wait-frame))


(defsub (exit-sequence-frame-1)
  (poke! palette-buffer 0 #x0f)
  (poke! palette-buffer 1 #x19)
  (poke! palette-buffer 2 #x19)
  (farcall palette-to-render-buffer-full)
  (set! ppu-ctrl (or (and ppu-ctrl #xfc) #x00))
  (wait-frame)
  (wait-frame))


(defsub (exit-sequence-frame-2)
  (poke! palette-buffer 0 #x19)
  (poke! palette-buffer 1 #x0f)
  (poke! palette-buffer 2 #x0f)
  (farcall palette-to-render-buffer-full)
  (set! ppu-ctrl (or (and ppu-ctrl #xfc) #x01))
  (wait-frame)
  (wait-frame))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Setup

(defconst nes-origin-player-v #x90)

(defsub (setup-the-game)
  ; Player
  (set! player-v nes-origin-player-v)
  (set! player-h #x68)
  ; PPU, same page for sprites and nametable
  (lda ppu-ctrl)
  (and #xe7)
  (sta ppu-ctrl)
  (sta REG-PPU-CTRL)
  ; Graphics
  (set-nt-pointer #x44)
  (nt-unrle)
  ; Entities
  (farcall clear-entities)
  (nes-add-spawner-entity)
  ; Key
  (set! nes-key-v #x20)
  (set! nes-key-h #x74)
  (set! nes-key-state 0)
  (set! nes-key-count 0)
  ; Ball
  (set! nes-mini-game-ball-speed
        (- (current-stage-number) nes-mini-game-easier))
  (when (>= nes-mini-game-ball-speed #x80)
    (set! nes-mini-game-ball-speed 0))
  ; NTSC / PAL speed
  (when tv-system
    (set! nes-mini-game-ball-speed (+ nes-mini-game-ball-speed 3)))
  ; Clear block page
  (nes-clear-blocks)
  ; Build blocks
  (nes-build-blocks)
  ; Turn on screen
  (enable-screen))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Top level

(defsub (play-the-game)
  (block Loop
    (wait-frame)
    (read-joypad)
    (clear-oam)
    (reset-sprite)
    (nes-handle-joypad)
    (nes-handle-jump)
    (nes-draw-player)
    (nes-update-objects)
    (when nes-mini-game-result
      (jmp #:break))
    (jmp Loop)))


(defsub (nes-handle-joypad)
  (when (and joypad-press button-a)
    (when (eq? nes-player-jump 0)
      (set! nes-player-jump 1)))
  (cond
   [(and joypad-data button-left)
      (inc nes-player-speed)
      (if (and joypad-data button-b)
          (nes-move-player 0 3)
          (if (>= nes-player-speed 10)
              (do (set! nes-player-speed 10)
                  (nes-move-player 0 2))
              (if (>= nes-player-speed 5)
                  (nes-move-player 0 1)
                  (nes-move-player 0 0))))]
   [(and joypad-data button-right)
      (inc nes-player-speed)
      (if (and joypad-data button-b)
          (nes-move-player 1 3)
          (if (>= nes-player-speed 10)
              (do (set! nes-player-speed 10)
                  (nes-move-player 1 2))
              (if (>= nes-player-speed 5)
                  (nes-move-player 1 1)
                  (nes-move-player 1 0))))]
   [else
      (set! nes-player-speed 0)])
  (when (< player-h #x12)
    (set! player-h #x12))
  (when (>= player-h #xbd)
    (set! player-h #xbc)))


;NTSC
; slow = 0:60
; med  = 1:20
; fast = 1:c0
; dash = 2:a0
;PAL
; slow = 0:74
; med  = 1:58
; fast = 2:18
; dash = 3:28


(defsub (nes-move-player dir speed)
  (let ((high) (low))
    (set! high 0) (set! low  0)
    (if (eq? tv-system 0)
        ; NTSC
        (cond
         [(eq? speed 0) (set! high 0) (set! low #x60)]
         [(eq? speed 1) (set! high 1) (set! low #x20)]
         [(eq? speed 2) (set! high 1) (set! low #xc0)]
         [(eq? speed 3) (set! high 2) (set! low #xa0)])
        ; PAL
        (cond
         [(eq? speed 0) (set! high 0) (set! low #x74)]
         [(eq? speed 1) (set! high 1) (set! low #x58)]
         [(eq? speed 2) (set! high 2) (set! low #x18)]
         [(eq? speed 3) (set! high 3) (set! low #x28)]))
    (when (eq? dir 0)
      ; Reverse direction
      (lda high)
      (eor #xff)
      (sta high)
      (lda low)
      (eor #xff)
      (clc)
      (adc 1)
      (sta low))
    ; Apply to player position
    (lda low)
    (clc)
    (adc player-h-low)
    (sta player-h-low)
    (lda high)
    (adc player-h)
    (sta player-h)))


(defsub (nes-update-objects)
  (nes-draw-key)
  (nes-update-entities))


(defsub (nes-handle-jump)
  (when (eq? nes-player-jump 0)
    (return))
  ; Set initial speed
  (when (eq? nes-player-jump 1)
    (set! nes-player-jlow #x00)
    (set! nes-player-jhigh #xfe))
  ; Count
  (inc nes-player-jump)
  ; End of jump
  (let ((delta))
    (set! delta (- player-v nes-origin-player-v))
    (when (and (< delta #x80) (>= delta 4))
      (set! player-v nes-origin-player-v)
      (set! nes-player-vlow 0)
      (set! nes-player-jump 0)
      (return)))
  ; Accelerate
  (lda nes-player-jlow)
  (clc)
  (adc #x20)
  (sta nes-player-jlow)
  (lda nes-player-jhigh)
  (adc 0)
  (sta nes-player-jhigh)
  ; Position
  (lda nes-player-vlow)
  (clc)
  (adc nes-player-jlow)
  (sta nes-player-vlow)
  (lda player-v)
  (adc nes-player-jhigh)
  (sta player-v))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Blocks (background hex)

(defsub (nes-clear-blocks)
  (lda 0)
  (ldy 0)
  (block ClearLoop
    (sta (addr #x700) y)
    (iny)
    (bne ClearLoop)))


(defsub (nes-build-blocks)
  ; 5 <= v < 13
  ; hstart >= 3, hend < 29
  (let ((stage))
    (set! stage (current-stage-number))
    (when (>= stage 1)
      (nes-insert-blocks  6 12 20))
    (when (>= stage 2)
      (nes-insert-blocks  6  4  6)
      (nes-insert-blocks  6 26 28)
      (nes-insert-blocks  8  7 11)
      (nes-insert-blocks  8 21 25))
    (when (>= stage 3)
      (nes-insert-blocks  7  6  8)
      (nes-insert-blocks  7 24 26)
      (nes-insert-blocks  8  6 12)
      (nes-insert-blocks  8 20 26)
      (nes-insert-blocks  9 15 17)
      (nes-insert-blocks 10 12 20))
    (when (>= stage 4)
      (nes-insert-blocks  5  3  6)
      (nes-insert-blocks  5 10 12)
      (nes-insert-blocks  5 20 22)
      (nes-insert-blocks  5 26 29)
      (nes-insert-blocks  6  3  6)
      (nes-insert-blocks  6 10 22)
      (nes-insert-blocks  6 26 29)
      (nes-insert-blocks  7  6 12)
      (nes-insert-blocks  7 20 26)
      (nes-insert-blocks  8  6 12)
      (nes-insert-blocks  8 20 26)
      (nes-insert-blocks  9 14 18)
      (nes-insert-blocks 10 12 20))
    (when (>= stage 5)
      (nes-insert-blocks  5  3 12)
      (nes-insert-blocks  5 20 29)
      (nes-insert-blocks  6  3 29)
      (nes-insert-blocks  7  6 26)
      (nes-insert-blocks  8  6 26)
      (nes-insert-blocks  9 12 20)
      (nes-insert-blocks 10 12 20))
    (when (>= stage 6)
      (nes-insert-blocks  9  9  13))))


(defsub (nes-insert-blocks vpos hstart hfinish)
  (let ((hpos))
    (set! hpos hstart)
    (block InsertLoop
      (nes-insert-block vpos hpos)
      (inc hpos)
      (when (not (eq? hpos hfinish))
        (jmp InsertLoop)))))


(defsub (nes-insert-block v h)
  (let ((n) (high) (low) (t))
    (set! t (random-get-hex))
    ; Fill nametable
    (set! low (+ (* #x20 v) h))
    (set! high (+ (/ v 8) #x20))
    (set! REG-PPU-ADDR high)
    (set! REG-PPU-ADDR low)
    (set! REG-PPU-DATA t)
    ; Fill RAM
    (set! n (+ (* (- v 5) #x20) h))
    (poke! (addr #x700) n t)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Random values

(defvarmem nes-player-speed  #x1c8)
(defvarmem nes-player-jump   #x104)
(defvarmem nes-player-jlow   #x105)
(defvarmem nes-player-jhigh  #x106)
(defvarmem nes-player-vlow   #x107)


(defsub (random-get-hex)
  (let ((n))
    (set! n (and (random-get) #x0f))
    (if (< n 10)
        (+ n #x30)
        (+ (- n 10) #x41))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Entities

(defconst entity-field-vlow #x10)
(defconst entity-field-hlow #x20)
(defconst entity-field-dir  #x50)


(defconst nes-entity-type-ball 1)
(defconst nes-entity-type-spawner 2)
(defconst nes-entity-type-fall-away-brick 3)


(defsub (get-available-obj)
  (loop x 0 16
        (when (eq? (peek entity-obj-data x) #xff)
          (txa)
          (sec)
          (return)))
  (ldx #xff)
  (clc))


(defsub (nes-add-spawner-entity)
  (block Alloc
    (set! entity-obj-num (get-available-obj))
    (bcs #:break)
    (return))
  (nes-entity-set-base)
  (poke! entity-obj-data (+ entity-obj-num entity-field-type)
         nes-entity-type-spawner)
  (poke! entity-obj-data (+ entity-obj-num entity-field-vpos) #x74)
  (poke! entity-obj-data (+ entity-obj-num entity-field-hpos) #x7c)
  (poke! entity-obj-data (+ entity-obj-num entity-field-dir)     8)
  (poke! entity-obj-data (+ entity-obj-num entity-field-step) #x68))


(defsub (nes-add-ball-entity)
  (block Alloc
    (set! entity-obj-num (get-available-obj))
    (bcs #:break)
    (return))
  (nes-entity-set-base)
  (poke! entity-obj-data (+ entity-obj-num entity-field-type)
         nes-entity-type-ball)
  (poke! entity-obj-data (+ entity-obj-num entity-field-vpos) #x74)
  (poke! entity-obj-data (+ entity-obj-num entity-field-hpos) #x7c)
  (poke! entity-obj-data (+ entity-obj-num entity-field-dir)     8))


(defconst entity-field-vspeed #x60)
(defconst entity-field-hspeed #x70)
(defconst entity-field-vspeed-low #x80)
(defconst entity-field-hspeed-low #x90)
(defconst entity-field-brick-tile #xa0)


(defsub (nes-add-fall-away-brick vpos hpos dir tile)
  (block Alloc
    (set! entity-obj-num (get-available-obj))
    (bcs #:break)
    (return))
  (nes-entity-set-base)
  (poke! entity-obj-data (+ entity-obj-num entity-field-type)
         nes-entity-type-fall-away-brick)
  (poke! entity-obj-data (+ entity-obj-num entity-field-vpos) vpos)
  (poke! entity-obj-data (+ entity-obj-num entity-field-hpos) hpos)
  ;
  (let ((vspeed) (vsl) (hspeed) (hsl))
    (asm "lda #<movement_1_8")
    (asm "sta tmp_pointer+0")
    (asm "lda #>movement_1_8")
    (asm "sta tmp_pointer+1")
    (lda dir)
    (asl a)
    (tay)
    (asm "lda (tmp_pointer),y")
    (sta hsl)
    (iny)
    (asm "lda (tmp_pointer),y")
    (sta hspeed)
    ; Add a quarter turn (TAU / 4) to get V dimension from H. Minus 1 for low.
    (tya)
    (clc)
    (adc 31)
    (tay)
    (asm "lda (tmp_pointer),y")
    (sta vsl)
    (iny)
    (asm "lda (tmp_pointer),y")
    (sta vspeed)
    ;
    (poke! entity-obj-data (+ entity-obj-num entity-field-vspeed) vspeed)
    (poke! entity-obj-data (+ entity-obj-num entity-field-vspeed-low) vsl)
    (poke! entity-obj-data (+ entity-obj-num entity-field-hspeed) hspeed)
    (poke! entity-obj-data (+ entity-obj-num entity-field-hspeed-low) hsl))
  ;
  (poke! entity-obj-data (+ entity-obj-num entity-field-brick-tile) tile))


(defsub (nes-entity-set-base)
  (poke! entity-obj-data (+ entity-obj-num entity-field-vlow) 0)
  (poke! entity-obj-data (+ entity-obj-num entity-field-hlow) 0))


(defsub (nes-update-entities)
  (let ((i) (type))
    (set! i 0)
    (block EntityLoop
      (set! type (peek entity-obj-data (+ i entity-field-type)))
      (when (eq? type #xff)
        (asm "jmp EntityNext"))
      ; It's the ball
      (nes-entity-update type i)
      ; Next
      (asm "EntityNext:")
      (inc i)
      (when (not (eq? i 16))
        (jmp EntityLoop)))))


(defsub (nes-entity-update type i)
  (cond
   [(eq? type nes-entity-type-spawner)
      (nes-entity-update-spawner i)]
   [(eq? type nes-entity-type-ball)
      (nes-entity-update-ball i)]
   [(eq? type nes-entity-type-fall-away-brick)
      (nes-entity-update-fall-away-brick i)]))


(defsub (nes-entity-update-spawner i)
  (let ((step) (n) (vpos) (hpos))
    (set! step (peek entity-obj-data (+ i entity-field-step)))
    (set! vpos (peek entity-obj-data (+ i entity-field-vpos)))
    (set! hpos (peek entity-obj-data (+ i entity-field-hpos)))
    (dec step)
    ; Set state
    (poke! entity-obj-data (+ i entity-field-step) step)
    ; Play warp ball FX
    (when (eq? step #x40)
      (audio-play-sfx audio-sfx-ball-warp))
    ; Wait
    (when (> step #x30)
      (return))
    ; Kill?
    (when (not step)
      (poke! entity-obj-data (+ i entity-field-type) #xff)
      (nes-add-ball-entity)
      (return))
    ; Flash when spawning
    (if (and step 2)
        (set! n (+ hpos (/ step 2)))
        (set! n (- hpos (/ step 2))))
    ; Draw
    (set-pointer! tmp-pointer nes-ball-gfx)
    (nes-draw-picture vpos n)))


(defsub (nes-entity-update-ball i)
  (let ((vpos) (hpos) (vlow) (hlow) (dir))
    ; State
    (set! vpos (peek entity-obj-data (+ i entity-field-vpos)))
    (set! hpos (peek entity-obj-data (+ i entity-field-hpos)))
    (set! vlow (peek entity-obj-data (+ i entity-field-vlow)))
    (set! hlow (peek entity-obj-data (+ i entity-field-hlow)))
    (set! dir  (peek entity-obj-data (+ i entity-field-dir)))
    ; Get speed pointer
    (cond
     [(eq? nes-mini-game-ball-speed 0)
        (asm "lda #<movement_1_6")
        (asm "ldx #>movement_1_6")]
     [(eq? nes-mini-game-ball-speed 1)
        (asm "lda #<movement_1_7")
        (asm "ldx #>movement_1_7")]
     [(eq? nes-mini-game-ball-speed 2)
        (asm "lda #<movement_1_8")
        (asm "ldx #>movement_1_8")]
     [(eq? nes-mini-game-ball-speed 3)
        (asm "lda #<movement_1_9")
        (asm "ldx #>movement_1_9")]
     [(eq? nes-mini-game-ball-speed 4)
        (asm "lda #<movement_2_0")
        (asm "ldx #>movement_2_0")]
     [(eq? nes-mini-game-ball-speed 5)
        (asm "lda #<movement_2_1")
        (asm "ldx #>movement_2_1")]
     [(eq? nes-mini-game-ball-speed 6)
        (asm "lda #<movement_2_2")
        (asm "ldx #>movement_2_2")]
     [(eq? nes-mini-game-ball-speed 7)
        (asm "lda #<movement_2_3")
        (asm "ldx #>movement_2_3")]
     [else
        (asm "lda #<movement_2_4")
        (asm "ldx #>movement_2_4")])
    (asm "sta tmp_pointer+0")
    (asm "stx tmp_pointer+1")
    ; Movement
    (lda dir)
    (asl a)
    (tay)
    (asm "lda (tmp_pointer),y")
    (clc)
    (adc hlow)
    (sta hlow)
    (iny)
    (asm "lda (tmp_pointer),y")
    (adc hpos)
    (sta hpos)
    ; Add a quarter turn (TAU / 4) to get V dimension from H. Minus 1 for low.
    (tya)
    (clc)
    (adc 31)
    (tay)
    (asm "lda (tmp_pointer),y")
    (clc)
    (adc vlow)
    (sta vlow)
    (iny)
    (asm "lda (tmp_pointer),y")
    (adc vpos)
    (sta vpos)
    ; Collision with outer walls
    (cond
     ; Bound off top wall
     [(and (< dir 32) (< vpos #x10))
        ; SFX
        (audio-play-sfx audio-sfx-ping-wall)
        ;
        (set! dir (- 64 dir))
        (set! dir (slightly-random-bounce dir))
        (inc vpos)]
     ; Bound off left wall
     [(and (>= dir 17) (< dir 48) (< hpos #x10))
        ; SFX
        (audio-play-sfx audio-sfx-ping-wall)
        ;
        (set! dir (- 32 dir))
        (set! dir (slightly-random-bounce dir))
        (inc hpos)]
     ; Bound off right wall
     [(and (or (< dir 16) (>= dir 48)) (>= hpos #xe8))
        ; SFX
        (audio-play-sfx audio-sfx-ping-wall)
        ;
        (set! dir (- 32 dir))
        (set! dir (slightly-random-bounce dir))
        (dec hpos)]
     ; Hit bottom wall, lose
     [(and (>= dir 33) (>= vpos #x9c))
        ; Fail due to losing ball.
        (audio-play-sfx audio-sfx-tragic)
        ; Delete the ball.
        (poke! entity-obj-data (+ i entity-field-type) #xff)
        ; Lose
        (set! nes-key-state 3)
        (inc nes-mini-game-easier)
        (return)])
    ; Collision with player
    (when (>= dir 33)
      ; Going in some downward direction.
      (let ((h-delta))
        ; Check horizontal delta, use this for amount of bounce as well.
        (set! h-delta (- hpos (+ player-h 24)))
        (when (or (> h-delta #xe2) (< h-delta #x18))
          (let ((v-delta))
            (set! v-delta (- vpos player-v))
            (when (or (< v-delta 7) (>= v-delta #xf7))
              ; Ball is either 9 pixels above or just below the bar
              ; Reflect so it's going upwards.
              ; SFX
              (audio-play-sfx audio-sfx-ping-pad)
              (set! dir (- 64 dir))
              (set! dir (bounce-based-upon-collision dir h-delta)))))))
    ; Collision with brick
    (let ((result))
      (set! nes-ball-dir dir)
      (set! result #f)
      (when (< dir 32)
        ; Moving up
        (set! result (ora result (nes-maybe-collide vpos hpos)))
        (set! result (ora result (nes-maybe-collide vpos (+ hpos 8))))
        (when result
          ; SFX
          (audio-play-sfx audio-sfx-ping-brick)
          ;
          (set! dir (- 64 dir))
          (set! dir (slightly-random-bounce dir))
          (inc vpos)))
      (when (and (>= dir 33) (not result))
        ; Moving down
        (set! result (ora result (nes-maybe-collide (+ vpos 8) hpos)))
        (set! result (ora result (nes-maybe-collide (+ vpos 8) (+ hpos 8))))
        (when result
          ; SFX
          (audio-play-sfx audio-sfx-ping-brick)
          ;
          (set! dir (- 64 dir))
          (set! dir (slightly-random-bounce dir))
          (dec vpos)))
      (when (and (>= dir 17) (< dir 48) (not result))
        ; Moving left
        (set! result (ora result (nes-maybe-collide vpos hpos)))
        (set! result (ora result (nes-maybe-collide (+ vpos 8) hpos)))
        (when result
          ; SFX
          (audio-play-sfx audio-sfx-ping-brick)
          ;
          (set! dir (- 32 dir))
          (set! dir (slightly-random-bounce dir))
          (inc hpos)))
      (when (and (or (< dir 16) (>= dir 48)) (not result))
        ; Moving right
        (set! result (ora result (nes-maybe-collide vpos (+ hpos 8))))
        (set! result (ora result (nes-maybe-collide (+ vpos 8) (+ hpos 8))))
        (when result
          ; SFX
          (audio-play-sfx audio-sfx-ping-brick)
          ;
          (set! dir (- 32 dir))
          (set! dir (slightly-random-bounce dir))
          (dec hpos))))
    ; Collision with key
    (let ((h-delta) (v-delta))
      (set! v-delta (- nes-key-v vpos))
      (set! h-delta (- nes-key-h hpos))
      (when (and (or (< v-delta 8) (>= v-delta #xf9))
                 (or (< h-delta 8) (>= h-delta #xe7)))
        ; SFX
        (audio-play-sfx audio-sfx-tada)
        ; Remove ball
        (poke! entity-obj-data (+ i entity-field-type) #xff)
        ; Flash key
        (set! nes-key-state 2)
        (return)))
    ; Direction
    (set! dir (and #x3f dir))
    ; Save state
    (poke! entity-obj-data (+ i entity-field-vpos) vpos)
    (poke! entity-obj-data (+ i entity-field-hpos) hpos)
    (poke! entity-obj-data (+ i entity-field-vlow) vlow)
    (poke! entity-obj-data (+ i entity-field-hlow) hlow)
    (poke! entity-obj-data (+ i entity-field-dir)  dir)
    ; Draw
    (set-pointer! tmp-pointer nes-ball-gfx)
    (nes-draw-picture vpos hpos)))


(defsub (nes-maybe-collide vpos hpos)
  (let ((v) (h) (n) (r) (high) (low))
    (set! v (/ vpos 8))
    (set! h (/ hpos 8))
    (when (in-range v 5 13)
      (set! n (+ (* (- v 5) #x20) h))
      (set! r (peek (addr #x700) n))
      (when r
        ; Clear the block from RAM
        (poke! (addr #x700) n 0)
        ; Calculate nametable change.
        (set! low (+ (* #x20 v) h))
        (set! high (+ (/ v 8) #x20))
        ; Render a change
        (poke! render-buffer render-chunk-index 1)
        (inc render-chunk-index)
        (poke! render-buffer render-chunk-index high)
        (inc render-chunk-index)
        (poke! render-buffer render-chunk-index low)
        (inc render-chunk-index)
        (poke! render-buffer render-chunk-index 0)
        (inc render-chunk-index)
        (set! render-chunk-has #t)
        ; Create a fall away block
        (nes-add-fall-away-brick (and vpos #xf8) (and hpos #xf8) nes-ball-dir r)
        #t))))


(defsub (bounce-based-upon-collision dir delta)
  (let ((target) (idx))
    ; Average with target direction.
    (set! idx (/ (+ delta #x1c) 8))
    (set! target (peek nes-bounce-table idx))
    (set! dir (/ (+ dir target) 2))))


(deflabel nes-bounce-table)
;      -3 -2 -1  0  1  2  3
(bytes 31 26 22 16 10  6  0 0)


(defsub (slightly-random-bounce dir)
  (let ((n))
    (set! n (- (and (random-get) 7) 3))
    (and (+ n dir) #x3f)))


(defsub (nes-entity-update-fall-away-brick i)
  (let ((vpos) (hpos) (vlow) (hlow) (dir) (tile) (vspeed) (vsl) (hspeed) (hsl))
    (set! vpos (peek entity-obj-data (+ i entity-field-vpos)))
    (set! hpos (peek entity-obj-data (+ i entity-field-hpos)))
    (set! vlow (peek entity-obj-data (+ i entity-field-vlow)))
    (set! hlow (peek entity-obj-data (+ i entity-field-hlow)))
    (set! tile (peek entity-obj-data (+ i entity-field-brick-tile)))
    (set! vspeed (peek entity-obj-data (+ i entity-field-vspeed)))
    (set! vsl    (peek entity-obj-data (+ i entity-field-vspeed-low)))
    (set! hspeed (peek entity-obj-data (+ i entity-field-hspeed)))
    (set! hsl    (peek entity-obj-data (+ i entity-field-hspeed-low)))

    ; Move
    (clc)
    (lda vsl)
    (adc vlow)
    (sta vlow)
    (lda vspeed)
    (adc vpos)
    (sta vpos)
    (clc)
    (lda hsl)
    (adc hlow)
    (sta hlow)
    (lda hspeed)
    (adc hpos)
    (sta hpos)

    ; Destroy
    (when (< vpos #x14)
      (poke! entity-obj-data (+ i entity-field-type) #xff)
      (return))
    (when (>= vpos #xa4)
      (poke! entity-obj-data (+ i entity-field-type) #xff)
      (return))
    (when (< hpos #x12)
      (poke! entity-obj-data (+ i entity-field-type) #xff)
      (return))
    (when (>= hpos #xea)
      (poke! entity-obj-data (+ i entity-field-type) #xff)
      (return))

    ; Gravity
    (clc)
    (lda vsl)
    (adc #x18)
    (sta vsl)
    (lda vspeed)
    (adc 0)
    (sta vspeed)

    ; Draw
    (get-sprite)
    (lda vpos)
    (sta (addr #x200) x)
    (lda tile)
    (sta (addr #x201) x)
    (lda 0)
    (sta (addr #x202) x)
    (lda hpos)
    (sta (addr #x203) x)

    (poke! entity-obj-data (+ i entity-field-vpos) vpos)
    (poke! entity-obj-data (+ i entity-field-hpos) hpos)
    (poke! entity-obj-data (+ i entity-field-vlow) vlow)
    (poke! entity-obj-data (+ i entity-field-hlow) hlow)

    (poke! entity-obj-data (+ i entity-field-vspeed)     vspeed)
    (poke! entity-obj-data (+ i entity-field-vspeed-low) vsl)
    (poke! entity-obj-data (+ i entity-field-hspeed)     hspeed)
    (poke! entity-obj-data (+ i entity-field-hspeed-low) hsl)

    ))


(asm ".include \"out/data/tables.asm\"")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Drawing pictures

(defsub (nes-draw-player)
  (set-pointer! tmp-pointer nes-player-gfx)
  (nes-draw-picture player-v player-h))


(defvarmem nes-key-v     #x100)
(defvarmem nes-key-h     #x101)
(defvarmem nes-key-state #x102)
(defvarmem nes-key-count #x103)


(defsub (nes-draw-key)
  (set-pointer! tmp-pointer nes-key-gfx)
  (cond
   [(eq? nes-key-state 0)
      ; spawning
      (inc nes-key-count)
      (cond
       [(< nes-key-count #x10)
          #f]
       [(< nes-key-count #x30)
          (when (and nes-key-count 8)
            (nes-draw-picture nes-key-v nes-key-h))]
       [(< nes-key-count #x58)
          (when (and nes-key-count 4)
            (nes-draw-picture nes-key-v nes-key-h))]
       [(< nes-key-count #x80)
          (when (and nes-key-count 2)
            (nes-draw-picture nes-key-v nes-key-h))]
       [(eq? nes-key-count #x80)
          ; done
          (set! nes-key-state 1)
          (set! nes-key-count 0)])]
   [(eq? nes-key-state 1)
      ; normal
      (nes-draw-picture nes-key-v nes-key-h)]
   [(eq? nes-key-state 2)
      ; win
      (inc nes-key-count)
      (when (and nes-key-count #x10)
        (nes-draw-picture nes-key-v nes-key-h))
      (when (>= nes-key-count #xa0)
        ; Exit the scene
        (set! nes-mini-game-easier 0)
        (set! nes-mini-game-result 2))]
   [(eq? nes-key-state 3)
      ; lose
      (inc nes-key-count)
      (when (eq? nes-key-count #xc0)
        (set! nes-mini-game-result 1))
      (nes-draw-picture nes-key-v nes-key-h)]))


(defsub (nes-draw-picture it-v it-h)
  (ldy 0)
  (block SpriteListLoop
    (asm "  lda (tmp_pointer),y")
    (beq #:break)
    (get-sprite)
    (lda it-v)
    (sta (addr #x200) x)
    (asm "  lda (tmp_pointer),y")
    (sta (addr #x201) x)
    (lda 0)
    (sta (addr #x202) x)
    (lda it-h)
    (sta (addr #x203) x)
    (set! it-h (+ it-h 8))
    (iny)
    (jmp SpriteListLoop)))


(deflabel nes-player-gfx)
(bytes "0XC0D3")
(bytes 0)


(deflabel nes-key-gfx)
(bytes #x21 #x13 #x4f 0)


(deflabel nes-ball-gfx)
(bytes #x6c 0)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defsub (final-mission)
  (clear-nametables)
  (set-nt-pointer #x4e)
  (nt-unrle)
  (enable-screen)
  (let ((msg) (count) (len))
    (set! msg 0)
    (set! count #xff)
    (block FinalLoop
      (wait-frame)
      (read-joypad)
      (when (and joypad-press button-a)
        (inc count)
        (when (>= count #x10)
          ; Upload complete!
          (set! len #x13)
          (memcpy render-buffer upload-complete-message len)
          (set! render-chunk-index len)
          (set! render-chunk-has #t)
          (jmp #:break))
        (when (not msg)
          (set! len #x24)
          (memcpy render-buffer uploading-message len)
          (set! render-chunk-index len)
          ; Fill bar a bit, top tile
          (poke! render-buffer render-chunk-index 1)
          (inc render-chunk-index)
          (poke! render-buffer render-chunk-index #x21)
          (inc render-chunk-index)
          (poke! render-buffer render-chunk-index (+ #x88 count))
          (inc render-chunk-index)
          (poke! render-buffer render-chunk-index #xca)
          (inc render-chunk-index)
          ; Fill bar a bit, bottom tile
          (poke! render-buffer render-chunk-index 1)
          (inc render-chunk-index)
          (poke! render-buffer render-chunk-index #x21)
          (inc render-chunk-index)
          (poke! render-buffer render-chunk-index (+ #xa8 count))
          (inc render-chunk-index)
          (poke! render-buffer render-chunk-index #xca)
          (inc render-chunk-index)
          (set! render-chunk-has #t)))
      (jmp FinalLoop))
    ; Done uploading
    (loop n 0 25
          (wait-frame))
    ; Glitch the freakin heck out.
    (audio-play-sfx audio-sfx-startup-cart)
    (music-play-song audio-tune-explosion)
    (let ((i) (j) (k) (m))
      (loop n 0 234
            (set! i (random-get))
            (set! j (random-get))
            (set! k (random-get))
            (set! m (/ i 8))
            (set! k (+ k m))
            (set! i (+ #x20 (and i 3)))
            (when (>= i #x23)
              (set! j (and #x3f j)))
            ; Write random char
            (poke! render-buffer render-chunk-index 1)
            (inc render-chunk-index)
            (poke! render-buffer render-chunk-index (+ #x20 (and i 3)))
            (inc render-chunk-index)
            (poke! render-buffer render-chunk-index j)
            (inc render-chunk-index)
            (poke! render-buffer render-chunk-index k)
            (inc render-chunk-index)
            (set! render-chunk-has #t)
            (wait-frame)))
    ; Trigger the final ending sequence.
    (set! progress-in-game progress-final-seq-0)
    (set! engine-mode 1)
    (set! generic-event 33)
    (travel-by-teleportation--wrapped maze-lab-scene final-seq-0-teleport)
    (set! shake-the-screen #x80)
    (disable-screen)
    ; Blow away the stack so we don't restore the player position.
    (unwind)))


(deflabel uploading-message)
(bytes #x0f)
(bytes #x22 #x08)
(bytes "UPLOADING" #x40 "VIRUS")
(bytes #x0f)
(bytes #x22 #x48)
(bytes #x40 #x40 #x40 #x40 #x40 #x40 #x40)
(bytes #x40 #x40 #x40 #x40 #x40 #x40 #x40)
(bytes #x40 #x40 #x40 #x40 #x40 #x40 #x40)


(deflabel upload-complete-message)
(bytes #x10)
(bytes #x22 #x08)
(bytes "UPLOAD" #x40 "COMPLETE" #x28)
