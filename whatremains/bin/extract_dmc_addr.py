import sys


def read_file(filename):
  fp = open(filename, 'r')
  content = fp.read()
  fp.close()
  return content


def run():
  filename = sys.argv[1]
  outfile = sys.argv[3]
  content = read_file(filename)
  address = None
  for line in content.split('\n'):
    if 'dmc_data:' in line:
      address = int(line[:5], 16)
  if not address:
    raise RuntimeError('Could not find address of dmc_data')
  fout = open(outfile, 'w')
  fout.write('DPCM_ADDR = $%04x\n\n' % address)
  fout.close()


if __name__ == '__main__':
  run()
