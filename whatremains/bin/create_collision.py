from PIL import Image
import sys
import StringIO


def yescolor(c):
  return c >= 0xe0


def nocolor(c):
  return c <= 0x30


def color_to_data(p):
  p = (p[0] & 0xf0, p[1] & 0xf0, p[2] & 0xf0)
  avg = (p[0] + p[1] + p[2]) / 3
  delta = abs(p[0] - avg) + abs(p[1] - avg) + abs(p[2] - avg)
  if delta < 48 and avg >= 0x90:
    # no collision
    return 0
  elif yescolor(p[0]) and nocolor(p[1]) and nocolor(p[2]):
    # full collision
    return 2
  elif yescolor(p[0]) and yescolor(p[1]) and nocolor(p[2]):
    # hide player in background
    return 1
  else:
    raise RuntimeError('Unknown color %s %s' % (p, delta))


def run():
  infile = sys.argv[1]
  if sys.argv[2] != '-o':
    raise RuntimeError('Expected: -o <output> --debug <debfile>')
  outfile = sys.argv[3]
  fout = open(outfile, 'w')
  # Debugging.
  debfile, dout = (None, None)
  if len(sys.argv) > 5 and sys.argv[4] == '--debug':
    debfile = sys.argv[5]
    dout = open(debfile, 'w')
  else:
    dout = StringIO.StringIO()
  img = Image.open(infile)
  width, height = img.size
  # Debugging top row
  dout.write('       h 00    h 04    h 08    h 0c    h 10    h 14    h 18    h 1c\n')
  # Rows of tiles
  for y in xrange(32):
    accum = []
    dout.write('v %02x |' % y)
    # Columns of 4x tiles
    for r in xrange(8):
      value = 0
      # Individual tiles within column
      for i in xrange(4):
        ypos = y * 8 + 2
        xpos = r * 32 + i * 8 + 2
        if ypos < height and xpos < width:
          pixel = img.getpixel((xpos, ypos))
          unit = color_to_data(pixel)
        else:
          unit = 2
        value |= (unit << (2 * i))
        dout.write('%d' % unit)
      accum.append(value)
      dout.write('=%02x|' % value)
    fout.write('.byte %s\n' % ','.join(['$%02x' % e for e in accum]))
    dout.write('\n')
  fout.close()
  dout.close()


if __name__ == '__main__':
  run()
