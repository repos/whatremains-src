#lang racket

(require racket/cmdline)

(define set-out (make-parameter #f))
(define set-alpha (make-parameter #f))
(define set-digit (make-parameter #f))
(define set-punc  (make-parameter #f))
(define set-border (make-parameter #f))
(define set-strip-font (make-parameter #f))

(define (fill-pattern pattern replace)
  (string-replace pattern "%s" replace))

(define (get-font-bytes size f strip-font?)
  (if strip-font?
      (make-bytes size 0)
      (read-bytes size f)))

(define (insert-chr in-chr-file out-chr-file alpha-file digit-file punc-file
                    strip-font? border-file)
  (let ((fin (open-input-file in-chr-file #:mode 'binary))
        (fa  (open-input-file alpha-file #:mode 'binary))
        (fd  (open-input-file digit-file #:mode 'binary))
        (fp  (open-input-file punc-file #:mode 'binary))
        (fb  (open-input-file border-file #:mode 'binary))
        (fout (open-output-file out-chr-file #:mode 'binary #:exists 'replace))
        (data #f))
    ; empty tile
    (set! data (make-bytes #x10 0))
    (write-bytes data fout)
    ; until tile $08
    (set! data (read-bytes #x70 fin))
    (write-bytes data fout)
    ; border, $08, $08
    (set! data (get-font-bytes #x080 fb strip-font?))
    (write-bytes data fout)
    ; until tile $10
    (set! data (read-bytes #x160 fin))
    (write-bytes data fout)
    ; punc, $26, $9
    (set! data (get-font-bytes #x90 fp strip-font?))
    (write-bytes data fout)
    ; until tile $30
    (set! data (read-bytes #x10 fin))
    (write-bytes data fout)
    ; digit, $30, $0a
    (set! data (get-font-bytes #x0a0 fd strip-font?))
    (write-bytes data fout)
    ; until tile $41
    (set! data (read-bytes #x070 fin))
    (write-bytes data fout)
    ; alpha, $41, $1a
    (set! data (get-font-bytes #x1a0 fa strip-font?))
    (write-bytes data fout)
    ;
    (set! data (read-bytes #xa50 fin))
    (write-bytes data fout)
    (close-input-port fin)
    (close-input-port fa)
    (close-input-port fd)
    (close-input-port fp)
    (close-input-port fb)
    (close-output-port fout)))

(define (pad-nametable in-nt-file out-nt-file)
  (let ((fin (open-input-file in-nt-file #:mode 'binary))
        (fout (open-output-file out-nt-file #:mode 'binary #:exists 'replace))
        (data #f)
        (b #f))
    (set! data (read-bytes #x400 fin))
    (for [(i (in-range (bytes-length data)))]
         (set! b (bytes-ref data i))
         (set! b (+ b 1))
         (when (>= b #x08)
               (set! b (+ b #x08)))
         (when (>= b #x26)
               (set! b (+ b #x09)))
         (when (>= b #x30)
               (set! b (+ b #x0a)))
         (when (>= b #x41)
               (set! b (+ b #x1a)))
         (bytes-set! data i b))
    (write-bytes data fout)
    (close-input-port fin)
    (close-output-port fout)))

(let* ((in-pattern (command-line
                    #:program "co2"
                    #:once-each
                    [("-o" "--output") out "output patttern" (set-out out)]
                    [("-A" "--alpha") name "alpha font" (set-alpha name)]
                    [("-D" "--digit") name "digit font" (set-digit name)]
                    [("-P" "--punc")  name "punc font"  (set-punc name)]
                    [("-B" "--border") name "border font" (set-border name)]
                    [("--strip-font") v "strip-font" (set-strip-font v)]
                    #:args (pattern) pattern))
       (alpha-file (set-alpha))
       (digit-file (set-digit))
       (punc-file  (set-punc))
       (border-file (set-border))
       (strip-font? (if (and (set-strip-font)
                             (string=? (set-strip-font) "true")) #t #f))
       (out-pattern (set-out)))
  (when (not out-pattern)
        (error "Expected, -o <output>"))
  (let* ((in-chr-file  (fill-pattern in-pattern "chr"))
         (in-nt-file   (fill-pattern in-pattern "nametable"))
         (in-nt1-file   (fill-pattern in-pattern "nametable1"))
         (in-pal-file  (fill-pattern in-pattern "palette"))
         (out-chr-file  (fill-pattern out-pattern "chr"))
         (out-nt-file   (fill-pattern out-pattern "nametable"))
         (out-nt1-file   (fill-pattern out-pattern "nametable1"))
         (out-pal-file  (fill-pattern out-pattern "palette")))
    (insert-chr in-chr-file out-chr-file alpha-file digit-file punc-file
                strip-font? border-file)
    (pad-nametable in-nt-file out-nt-file)
    (when (file-exists? in-nt1-file)
          (pad-nametable in-nt1-file out-nt1-file))))
