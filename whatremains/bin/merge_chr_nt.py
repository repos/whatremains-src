import argparse
from makechr import chr_data
from makechr.gen import valiant_pb2 as valiant
from PIL import Image
import sys


def save_output(filename, data):
  fp = open(filename, 'wb')
  fp.write(bytearray(data))
  fp.close()


def fill_template(template, num):
  return template.replace('%d', '%02d' % num)


def kind_to_role(kind):
  if kind == 'chr':
    return valiant.CHR
  elif kind == 'nametable':
    return valiant.NAMETABLE
  elif kind == 'attribute':
    return valiant.ATTRIBUTE
  elif kind == 'palette':
    return valiant.PALETTE
  else:
    raise RuntimeError('Unknown kind %s' % kind)


def get_bytes(obj, kind, expand=False):
  role = kind_to_role(kind)
  for packet in obj.body.packets:
    if packet.role == role:
      binary = packet.binary
      break
  else:
    return bytearray()
  data = binary.bin
  if expand:
    if binary.pre_pad:
      data = bytearray([binary.null_value] * binary.pre_pad) + data
    if binary.padding:
      data = data + bytearray([binary.null_value] * binary.padding)
  return bytearray(data)


def read_chr_file(filename):
  fp = open(filename, 'r')
  content = fp.read()
  fp.close()
  empty_tile = bytearray([0]*16)
  i = len(content)
  while True:
    if i - 16 <= 0:
      break
    if content[i-16:i] == empty_tile:
      i -= 16
    else:
      break
  return bytearray(content[0:i])


def build_xlat(left_chr_page, right_chr_page):
  # Build translator so we can update the new nametable.
  i = j = 0
  xlat = {}
  while i < left_chr_page.num_idx() and j < right_chr_page.num_idx():
    c = left_chr_page.k_smallest(i)
    d = right_chr_page.k_smallest(j)
    if c < d:
      # Chr tile that only exists in left.
      i += 1
    elif c == d:
      # Chr tile matches, assign the new index.
      xlat[right_chr_page.index(j)] = left_chr_page.index(i)
      i += 1
      j += 1
    else: # c > d
      # Chr tile that is new, only exists in right.
      j += 1
  n = left_chr_page.size()
  for k in xrange(right_chr_page.size()):
    if not k in xlat:
      xlat[k] = n
      n += 1
  return xlat, n


def combine_chr(left_chr_page, right_chr_page, xlat):
  n = left_chr_page.size()
  j = n
  for i in xrange(right_chr_page.size()):
    if not i in xlat:
      continue
    k = xlat[i]
    if k >= n:
      left_chr_page.add(right_chr_page.get(i))
      j += 1


def perform_merge(left_chr_page, right_chr_page, nametable):
  (xlat, n) = build_xlat(left_chr_page, right_chr_page)
  combine_chr(left_chr_page, right_chr_page, xlat)
  for i,t in enumerate(nametable):
    try:
      nametable[i] = xlat[t]
    except KeyError:
      nametable[i] = 0xff
  return nametable


def insert_pad(data, padding):
  make = bytearray([0] * len(data))
  for n, b in enumerate(data):
    i = b
    for p, k in padding:
      if i >= p:
        i += k
    try:
      make[n] = i
    except:
      sys.stderr.write('Error setting %s <- %s\n' % (n, i))
  return make


def add_charset_padding(padding, charset):
  for ch in charset:
    padding.append([ord(ch), 1])
  return padding


def insert_chr_data_from_charset(data, alpha_obj, charset):
  alpha_chr = get_bytes(alpha_obj, 'chr')
  for ch in charset:
    n = ord(ch)
    k = n - 0x41
    data = data[:0x10 * n] + alpha_chr[k*0x10:k*0x10+0x10] + data[0x10 * n:]
  return data


def insert_font_bytes(data, chr_obj, offset, size, strip_font):
  if not chr_obj:
    return data
  if strip_font:
    return data[:offset] + bytearray([0]*size) + data[offset:]
  else:
    return data[:offset] + get_bytes(chr_obj, 'chr')[:size] + data[offset:]


_g_why_this_hack = False


def merge_objects(input_files, alpha_obj, digit_obj, punc_obj, border_obj,
                  charset, strip_font, out_chr_name, out_palette_name,
                  out_nt_tmpl, out_attr_tmpl):
  padding = []
  padding.append([0, 2])
  global _g_why_this_hack
  if _g_why_this_hack:
    padding = []
  if border_obj:
    padding.append([0x08, 0x08])
  if punc_obj:
    padding.append([0x26, 0x09])
  if digit_obj:
    padding.append([0x30, 0x0a])
  if alpha_obj and not charset:
    padding.append([0x41, 0x1a])
  if alpha_obj and charset:
    padding = add_charset_padding(padding, charset)
  filename = input_files[0]
  if filename.endswith('.o'):
    obj = parse_object_file(filename)
    chr_bin = get_bytes(obj, 'chr')
    nametable = get_bytes(obj, 'nametable', expand=True)
    attribute = get_bytes(obj, 'attribute', expand=True)
    save_output(fill_template(out_nt_tmpl, 0), insert_pad(nametable, padding))
    save_output(fill_template(out_attr_tmpl, 0), attribute)
    palette = get_bytes(obj, 'palette', expand=True)
    if out_palette_name:
      save_output(out_palette_name, palette)
  else:
    chr_bin = read_chr_file(filename)
  combined_chr_page = chr_data.SortableChrPage.from_binary(str(chr_bin))
  for i,filename in enumerate(input_files):
    if i == 0:
      continue
    obj = parse_object_file(filename)
    chr_bin = get_bytes(obj, 'chr')
    nametable = get_bytes(obj, 'nametable', expand=True)
    attribute = get_bytes(obj, 'attribute', expand=True)
    right_chr_page = chr_data.SortableChrPage.from_binary(str(chr_bin))
    perform_merge(combined_chr_page, right_chr_page, nametable)
    save_output(fill_template(out_nt_tmpl, i), insert_pad(nametable, padding))
    save_output(fill_template(out_attr_tmpl, i), attribute)
  data = (bytearray([0]*16) +
          bytearray([0xff]*8) + bytearray([0]*8) +
          combined_chr_page.to_bytes())
  if _g_why_this_hack:
    data = data[32:]
  data = insert_font_bytes(data, border_obj, 0x80, 0x80, strip_font)
  data = insert_font_bytes(data, punc_obj,  0x260, 0x90, strip_font)
  data = insert_font_bytes(data, digit_obj, 0x300, 0xa0, strip_font)
  data = insert_font_bytes(data, alpha_obj, 0x410, 0x1a0, strip_font)
  data = data + bytearray([0] * (0x2000 - len(data)))
  if out_chr_name:
    save_output(out_chr_name, data)


def parse_object_file(filename):
  if filename is None:
    return None
  fp = open(filename, 'rb')
  content = fp.read()
  fp.close()
  if content[0:9] != '(VALIANT)':
    raise RuntimeError('Could not parse file: %s' % f)
  obj = valiant.ObjectFile()
  obj.ParseFromString(content)
  return obj


def process(input_files, alpha_input_file, digits_input_file, punc_input_file,
            border_input_file, strip_font, charset, out_chr_name,
            out_palette_name, out_nt_tmpl, out_attr_tmpl):
  alpha_obj = parse_object_file(alpha_input_file)
  digit_obj = parse_object_file(digits_input_file)
  punc_obj = parse_object_file(punc_input_file)
  border_obj = parse_object_file(border_input_file)
  merge_objects(input_files, alpha_obj, digit_obj, punc_obj, border_obj,
                charset, strip_font, out_chr_name, out_palette_name,
                out_nt_tmpl, out_attr_tmpl)


def run():
  parser = argparse.ArgumentParser()
  parser.add_argument('input', type=str, nargs='+')
  parser.add_argument('-c', dest='chr')
  parser.add_argument('-p', dest='palette')
  parser.add_argument('-n', dest='nametable')
  parser.add_argument('-a', dest='attribute')
  parser.add_argument('-A', dest='alpha')
  parser.add_argument('-D', dest='digits')
  parser.add_argument('-C', dest='charset')
  parser.add_argument('-P', dest='punc')
  parser.add_argument('-B', dest='border')
  parser.add_argument('-y', dest='why_this_hack', action='store_true')
  parser.add_argument('--strip-font', dest='strip_font', action='store_true')
  args = parser.parse_args()
  if args.why_this_hack:
    global _g_why_this_hack
    _g_why_this_hack = True
  process(args.input, args.alpha, args.digits, args.punc, args.border,
          args.strip_font, args.charset, args.chr, args.palette,
          args.nametable, args.attribute)


if __name__ == '__main__':
  run()
