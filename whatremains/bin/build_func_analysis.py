import collections
import sys
import re


CallDetail = collections.namedtuple('CallDetail', ['in_call', 'under_call', 'depends'])


def read_call_graph(filename):
  fp = open(filename, 'r')
  content = fp.read()
  fp.close()
  result = {}
  header = True
  for line in content.split('\n'):
    if line.startswith('======='):
      header = False
      continue
    if header:
      continue
    if not line:
      continue
    parts = line.split(' ')
    name, in_call, under_call, rest = parts[0], parts[2], parts[4], parts[5:]
    result[name] = CallDetail(in_call, under_call, len(rest) > 0)
  return result


def read_func_addrs(filename):
  fp = open(filename, 'r')
  content = fp.read()
  fp.close()
  result = []
  for line in content.split('\n'):
    if not line:
      continue
    parts = line.split(' ')
    addr, size, name = int(parts[0]), int(parts[1]), parts[2]
    result.append([addr, size, name])
  return result


def process():
  call_graph = read_call_graph('.analyze/call-graph.txt')
  func_addrs = read_func_addrs('.analyze/func-addrs.txt')
  for elem in func_addrs:
    addr, size, name = elem
    details = call_graph.get(name)
    if details is None:
      details = CallDetail('', '', '')
    print('%04x %4x %34s %06s %06s %s' % (
      addr, size, name, details.in_call, details.under_call,
      ' *DATA*' if details.depends else ''))


if __name__ == '__main__':
  process()
