import sys


def run():
  infile = sys.argv[1]
  if sys.argv[2] != '-o':
    raise RuntimeError('Expected: -o <outfile>')
  outfile = sys.argv[3]
  fp = open(infile, 'rb')
  content = fp.read()
  fp.close()
  content = bytearray([chr(ord(e) ^ 0xff) for e in content])
  fout = open(outfile, 'wb')
  fout.write(content)
  fout.close()


if __name__ == '__main__':
  run()
