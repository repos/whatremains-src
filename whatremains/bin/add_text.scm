#lang racket

(require racket/cmdline)

(define set-out (make-parameter #f))
(define set-in (make-parameter #f))
(define set-other (make-parameter #f))

(define (apply-xform xf data)
  (let* ((result (regexp-match #px"^([\\d]+),([\\d]+):(.*)$" xf))
         (v-pos (string->number (list-ref result 1)))
         (h-pos (string->number (list-ref result 2)))
         (text  (string->bytes/latin-1 (list-ref result 3)))
         (i     (+ (* v-pos #x20) h-pos))
         (b #f))
    (for [(k (in-range (bytes-length text)))]
         (set! b (bytes-ref text k))
         (cond
          [(eq? b #x20) (set! b 0)]
          [(eq? b #x28)  (set! b #x0c)]
          [(eq? b #x29)  (set! b #x0d)]
          [(eq? b #x2e)  (set! b #x2d)])
         (bytes-set! data (+ i k) b))))

(define (all-add-text in-file out-file xforms)
  (let ((fin (open-input-file in-file #:mode 'binary))
        (fout (open-output-file out-file #:mode 'binary #:exists 'replace))
        (data #f)
        (b #f))
    (set! data (read-bytes #x400 fin))
    (for [(xf xforms)]
         (apply-xform xf data))
    (write-bytes data fout)
    (close-input-port fin)
    (close-output-port fout)))

(let* ((xforms (command-line
                #:program "co2"
                #:once-each
                [("-o" "--output") out "output patttern" (set-out out)]
                [("-i" "--input") in "input patttern" (set-in in)]
                #:args xforms xforms))
       (out-file (set-out))
       (in-file (set-in)))
  (when (or (not out-file) (not in-file))
        (error "Expected, -o <output> -i <input>"))
  (all-add-text in-file out-file xforms))
