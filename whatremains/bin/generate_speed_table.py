import fixed_point
import math


TAU = 6.283185307179586
NUM_DIRS = 64


def speed_to_label(speed):
  if speed == 0:
    return '0'
  return 'movement_%s' % ('%s' % speed).replace('.', '_')


def move_at_click(angle, speed):
  movement = math.cos(angle * TAU / NUM_DIRS) * speed
  return fixed_point.FixedPoint.from_float(movement)


def round_off(num, bits):
  if num < 0:
    num += 256
  if bits != 3:
    raise RuntimeError('Implement me')
  lower = num & 0x03
  if lower < 2:
    num = num & 0xfc
  else:
    num = (num & 0xfc) + 0x04
  return num


def make_byte(num):
  if num is None:
    return None
  if num < 0:
    num += 0x100
  if num >= 0x100:
    num -= 0x100
  return int(num)


def table_new(text):
  print('%s:' % text)


def table_out(vals, slice_size):
  num_slices = int(math.ceil(1.0 * len(vals) / slice_size))
  for i in xrange(num_slices):
    slice = vals[i*slice_size:i*slice_size + slice_size]
    print('.byte %s' % ','.join(['$%02x' % e for e in slice]))
  print('')


def generate_movement(speed):
  vals = []
  for angle in range(NUM_DIRS + NUM_DIRS / 4):
    delta = move_at_click(angle, speed)
    row = [round_off(delta.low, 3), delta.high]
    vals = vals + [make_byte(n) for n in row]
  movement_label = speed_to_label(speed)
  table_new(movement_label)
  table_out(vals, 2)


if __name__ == '__main__':
  generate_movement(1.6) # ntsc     pal
  generate_movement(1.7) # stage 1
  generate_movement(1.8) # stage 2
  generate_movement(1.9) # stage 3
  generate_movement(2.0) # stage 4  stage 1
  generate_movement(2.1) # stage 5  stage 2
  generate_movement(2.2) #          stage 3
  generate_movement(2.3) #          stage 4
  generate_movement(2.4) #          stage 5
