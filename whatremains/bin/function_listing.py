# python bin/function_listing.py .b/main.lst

import sys
import re


regex = r'^([\w]{5}).*\(def(sub|vector) \(([\w-]+)'


def process(filename):
  fp = open(filename, 'r')
  content = fp.read()
  fp.close()
  accum = []
  for line in content.split('\n'):
    m = re.match(regex, line)
    if m:
      addr, fname = m.group(1), m.group(3)
      addr = int(addr[1:].lower(), 16)
      accum.append([addr, 0, fname])
  for i in xrange(len(accum) - 1):
    addr, size, fname = accum[i]
    ending = accum[i + 1][0]
    size = ending - addr
    accum[i][1] = size
  for addr, size, fname in accum:
    print addr, size, fname


if __name__ == '__main__':
  filename = sys.argv[1]
  process(filename)
