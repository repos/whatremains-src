#lang racket

(require racket/cmdline)

(define set-in    (make-parameter #f))
(define set-out   (make-parameter #f))
(define set-width (make-parameter #f))
(define set-pal   (make-parameter #f))
(define set-build (make-parameter #f))
(define set-invert (make-parameter #f))
(define set-rgb-mapping (make-parameter #f))

(define (fill-pattern pattern replace)
  (string-replace pattern "%s" replace))

(define (path->basename p)
  (path->string (path-replace-extension (file-name-from-path p) "")))

(define run-cmd
  (lambda args
    (let* ((bin  (find-executable-path (car args)))
           (rest (cdr args))
           (val  #f))
      (printf "\n***********************\n* ~a\n" (cons bin rest))
      (set! val (apply system* (cons bin rest)))
      (when (not val)
            (error "Command failed")))))

(define (makechr-dep img-file out-chr)
  (when (or (not (file-exists? out-chr))
            (> (file-or-directory-modify-seconds img-file)
               (file-or-directory-modify-seconds out-chr)))
    (run-cmd "makechr" img-file "-o" (string-replace out-chr "chr" "%s"))))

(define (bin-cat left rite out)
  (let ((fleft (open-input-file left #:mode 'binary))
        (frite (open-input-file rite #:mode 'binary))
        (fout (open-output-file out #:mode 'binary #:exists 'replace)))
    (write-bytes (port->bytes fleft) fout)
    (write-bytes (port->bytes frite) fout)
    (close-output-port fout)))

(define (bin-cat-4 bin0 bin1 bin2 bin3 out)
  (let ((fin0 (open-input-file bin0 #:mode 'binary))
        (fin1 (open-input-file bin1 #:mode 'binary))
        (fin2 (open-input-file bin2 #:mode 'binary))
        (fin3 (open-input-file bin3 #:mode 'binary))
        (fout (open-output-file out #:mode 'binary #:exists 'replace)))
    (write-bytes (port->bytes fin0) fout)
    (write-bytes (port->bytes fin1) fout)
    (write-bytes (port->bytes fin2) fout)
    (write-bytes (port->bytes fin3) fout)
    (close-output-port fout)))

(define (rle in-file out-file)
  (run-cmd "python" "bin/graphics_compress.py" in-file "-o" out-file))

(let* ((unused (command-line
                #:program "create_rle_image"
                #:once-each
                [("-w" "--width")  width "width" (set-width width)]
                [("-p" "--palette") pal  "pal" (set-pal pal)]
                [("-o" "--output") out   "output pattern" (set-out out)]
                [("-i" "--input")  in    "input image" (set-in in)]
                [("--invert-chr") invert "invert chr" (set-invert invert)]
                [("-b" "--build")  build "build dir" (set-build build)]
                [("--rgb-mapping") map "rgb" (set-rgb-mapping map)]))
       (out-pattern (set-out))
       (in-file     (set-in))
       (build-dir   (set-build))
       (width-gfx   (if (set-width) (string->number (set-width)) #f))
       (should-invert (set-invert))
       (palette     (set-pal))
       (basename    (path->basename (string->path in-file)))
       (no-text-pattern (string-append build-dir basename "-notext.%s.bin"))
       (bin-pattern (string-append build-dir basename ".%s.bin"))
       (rgb-mapping (set-rgb-mapping)))
  (when (not out-pattern)
        (error "Expected, -o <output>"))
  (when (not rgb-mapping)
        (set! rgb-mapping "almighty"))
  (makechr-dep "assets/graphics/alpha.png" "out/graphics/alpha.chr.bin")
  (makechr-dep "assets/graphics/digit.png" "out/graphics/digit.chr.bin")
  (makechr-dep "assets/graphics/punc.png"  "out/graphics/punc.chr.bin")
  (makechr-dep "assets/graphics/border.png"  "out/graphics/border.chr.bin")
  (if palette
      (run-cmd "makechr" in-file "-o" no-text-pattern "-p" palette
               "--rgb-mapping" rgb-mapping)
      (run-cmd "makechr" in-file "-o" no-text-pattern
               "--rgb-mapping" rgb-mapping))
  (if (not should-invert)
      (run-cmd "racket" "bin/merge_chr_nt.scm" "-o" bin-pattern
               "-A" "out/graphics/alpha.chr.bin"
               "-D" "out/graphics/digit.chr.bin"
               "-P" "out/graphics/punc.chr.bin"
               "-B" "out/graphics/border.chr.bin"
               "--strip-font" "true"
               no-text-pattern)
      (run-cmd "racket" "bin/merge_chr_nt.scm" "-o" bin-pattern
               "-A" "out/graphics/alpha-invert.chr.bin"
               "-D" "out/graphics/digit-invert.chr.bin"
               "-P" "out/graphics/punc-invert.chr.bin"
               "-B" "out/graphics/border-invert.chr.bin"
               "--strip-font" "true"
               no-text-pattern))
  (if (eq? width-gfx 2)
    (bin-cat-4 (fill-pattern bin-pattern "nametable")
               (fill-pattern no-text-pattern "attribute")
               (fill-pattern bin-pattern "nametable1")
               (fill-pattern no-text-pattern "attribute1")
               (fill-pattern bin-pattern "gfx"))
    (bin-cat (fill-pattern bin-pattern "nametable")
             (fill-pattern no-text-pattern "attribute")
             (fill-pattern bin-pattern "gfx")))
  (copy-file (fill-pattern no-text-pattern "palette")
             (fill-pattern bin-pattern "palette") #t)
  (rle (fill-pattern bin-pattern "gfx") (fill-pattern out-pattern "gfx"))
  (rle (fill-pattern bin-pattern "chr") (fill-pattern out-pattern "chr")))
