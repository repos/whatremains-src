import sys

def read_file(filename):
  fp = open(filename, 'rb')
  content = fp.read()
  fp.close()
  return bytearray(content)

def write_file(contents, filename):
  fp = open(filename, 'wb')
  fp.write(contents)
  fp.close()

def poke_value(input, val0, val1, filename, suffix):
  data = input[:]
  pos = 0
  while True:
    pos = data.find('DEVOPTS', pos+1)
    if pos == -1:
      break
    offset = len('DEVOPTS')
    data[pos + offset + 0] = val0
    data[pos + offset + 1] = val1
  write_file(data, filename.replace('.nes', suffix + '.nes'))

def run():
  filename = sys.argv[1]
  contents = read_file(filename)
  poke_value(contents, 0, 0, filename, '-release')
  poke_value(contents, 0, 1, filename, '-exhibit')

if __name__ == '__main__':
  run()
