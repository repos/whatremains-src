mkdir -p .analyze
python bin/function_listing.py .b/main.lst > .analyze/func-addrs.txt
racket co2.scm -o out/bin/main.nes --analyze-calls src/main.co2 > .analyze/call-graph.txt
python bin/build_func_analysis.py
