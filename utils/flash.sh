#!/bin/sh
READNESS="/home/a/v/nesutils-1.0/readnes"

if [ "$1" = "local" ]
then
	echo "local build"
	cp ../whatremains/whatremains.nes /tmp/lastbuild.rom	
else
	LASTBUILD=$(ls ../builds | sort -n | tail -1)
	echo ${LASTBUILD}
	cp ../builds/${LASTBUILD} /tmp/lastbuild.rom
fi

${READNESS} /tmp/lastbuild.rom 1 0

#minipro -p "M27C4001 @DIP32" -w /tmp/lastbuild-padded.prg
minipro -p "M27C4001@DIP32" -w /tmp/lastbuild.prg

rm /tmp/lastbuild*
