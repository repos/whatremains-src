# neovim plugin to turn raw dialog from wiki into code for dialog.co2
# Usage: 
# 1. paste raw dialog into dialog.co2
# 2. make visual selection of the raw dialog *with* last line break
# 3. |:MakeDialog name-for-deflabel
# 4. ????
# 5. PROFIT!!!

import neovim

def send_help(something):
	with open('/tmp/help.log', 'a') as log:
		log.write(format(something))
		log.write('\n\n***\n\n')
		log.close()


def make_dialog(text):
	counter = 0
    
	for index, value in enumerate(text):
		if value != '':
			if text[index + 1] != '':
				text[index] = '(bytes "' + value + '" endl)'
			else:
				text[index] = '(bytes "' + value + '")'
			counter += 1
		else:
			if index != len(text) - 1:
				if counter <= 2:
					#text[index] = '(bytes "jc")'
					text[index] = '(bytes "j")'
					counter = 0
				else:
					#text[index] = '(bytes "jcd")'
					text[index] = '(bytes "j")'
					counter = 0
			else:
				#text[index] = '(bytes "j" 0)'
				text[index] = '(bytes 0)'
				
	return text


@neovim.plugin
class MakeDialogPlugin(object):

	def __init__(self, nvim):
		self.nvim = nvim

	@neovim.command("MakeDialog", range='', nargs='1')
	def command(self, args, range):
		start, end = range
		source = self.nvim.current.buffer[start-1:end]
		
		self.nvim.call("feedkeys", "o")
		self.nvim.call("feedkeys", "k")
		
		dialog = make_dialog(source)

		self.nvim.current.line = '(deflabel ' + args[0] + ')'
		self.nvim.current.buffer[start:end] = dialog
